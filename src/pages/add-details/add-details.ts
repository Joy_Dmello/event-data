import { Component,ViewChild,ElementRef,Renderer } from '@angular/core';
import { IonicPage, NavController, NavParams,Searchbar } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Toast } from '@ionic-native/toast';
import {Keyboard} from 'ionic-native';

/**
* Generated class for the AddDetailsPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-add-details',
  templateUrl: 'add-details.html',
})
export class AddDetailsPage {

  @ViewChild('keywords_input') input:ElementRef;

  DataForm: FormGroup;
  email_count:Number =0;
  mobile_count:Number =0;
  name_count:Number =0;
  designation:String;
  speciality:String;
  emirates:String;
  select_focus:String;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public sqlite: SQLite,
    private toast: Toast,
    public formBuilder: FormBuilder,
    private elementRef:ElementRef,
    private renderer:Renderer
    ) {

      this.DataForm = formBuilder.group({
        // title:['',Validators.compose([])],
        name: ['', Validators.compose([Validators.required])],
        email_id: ['',Validators.compose([Validators.required, Validators.pattern("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$")])],
        mobile_no: ['', Validators.compose([Validators.required])],
        hospital_name: ['', Validators.compose([Validators.required])],
        designation: ['', Validators.compose([Validators.required])],
        speciality: ['', Validators.compose([Validators.required])],
        nationality: ['United Arab Emirates', Validators.compose([Validators.required])],
        country: ['United Arab Emirates', Validators.compose([Validators.required])],
        other_nation:['',Validators.compose([])],
        other_country:['',Validators.compose([])],      
        emirates: ['', Validators.compose([Validators.required])],
        comments: ['', Validators.compose([])]
      });





      this.DataForm.get('country').valueChanges.subscribe(
          (country: string) => {
              if (country === 'OTHERS') {
                  this.DataForm.get('other_country').setValidators([Validators.required]);                
              }else {
                this.DataForm.get('other_country').setValidators([]);
              }
              this.DataForm.get('other_country').updateValueAndValidity();
          }
        )

      this.DataForm.get('nationality').valueChanges.subscribe(
          (nationality: string) => {
              if (nationality === 'OTHERS') {
                  this.DataForm.get('other_nation').setValidators([Validators.required]);                
              }else {
                this.DataForm.get('other_nation').setValidators([]);
              }
              this.DataForm.get('other_nation').updateValueAndValidity();
          }
        )
    }

    setSelectfocus(id,field=""){
      // this.emirates_field.setFocus();

      this.select_focus = id;

      // if(id=="none"){
      //   // const element = this.elementRef.nativeElement.querySelector('keywords_input');
      //   // we need to delay our call in order to work with ionic ...
      //   setTimeout(() => {
      //       // console.log(element);
      //       this.renderer.invokeElementMethod(this.input, 'focus', []);
      //       Keyboard.show();
      //   }, 20);
      // }

    }
  

  titles(): string[] {
      return [
        "MR","MS","Mrs","Dr","Prof"]
    }

    title: string = "MR";

    countries():string[]{
      return [
      "Afghanistan","Algeria","Armenia","Azerbaijan","Bahrain","Cyprus","Egypt","India",
      "Iran","Iraq","Jordan","Kazakhstan","Kuwait","Kyrgyzstan","Lebanon","Libya","Morocco",
      "Oman","Pakistan","Palestine","Qatar","Saudi Arabia","Sudan","Syria","Tunisia","Turkey",
      "Turkmenistan","United Arab Emirates","Uzbekistan","Yemen","OTHERS"]
    }


    nations():string[]{
      return [
        "Afghanistan","Aland Islands","Albania","Algeria","American Samoa","Andorra", "Angola","Anguilla","Antarctica",
        "Antigua and Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh",
        "Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia","Botswana","Bouvet Island","Brazil",
        "British Indian Ocean Territory","Brunei Darussalam","Bulgaria","Burkina Faso","Burundi","Cabo Verde","Cambodia","Cameroon",
        "Canada","Cayman Islands","Central African Republic","Chad","Chile","China","Christmas Island","Cocos Islands","Colombia",
        "Comoros","Congo","Cook Islands","Costa Rica","Cote d'Ivoire","Croatia","Cuba","Curaçao","Cyprus","Czech Republic","Denmark",
        "Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia",
        "Falkland Islands","Faroe Islands","Fiji","Finland","France","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece",
        "Greenland","Grenada","Guadeloupe","Guam","Guernsey","Guiana","Guinea","Guyana","Haiti","Holy See","Honduras","Hong Kong",
        "Hungary","Iceland","India","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan",
        "Kazakhstan","Kenya","Kiribati","Korea North","Korea South","Kuwait","Kyrgyzstan","Latvia","Lebanon","Lesotho","Liberia","Libya",
        "Liechtenstein","Lithuania","Luxembourg","Macao","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands",
        "Martinique","Mauritania","Mauritius","Mayotte","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat",
        "Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria",
        "Niue","Norfolk Island","Northern Mariana Islands","Norway","Oman","Pakistan","Palau","Palestine","State of Panama","Papua New Guinea",
        "Paraguay","Peru","Philippines","Pitcairn","Poland","Polynesia","Portugal","Puerto Rico","Qatar","Republic of Kosovo","Réunion",
        "Romania","Russian Federation","Rwanda","Saint Barthélemy","Saint Kitts and Nevis","Saint Lucia","Saint Martin","Saint Pierre and Miquelon",
        "Saint Vincent and the Grenadines","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles",
        "Sierra Leone","Singapore","Sint Maarten (Dutch part)","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Georgia" ,
        "Spain","Sri Lanka","Sudan","Suriname","Svalbard and Jan Mayen","Swaziland","Sweden","Switzerland","Syrian Arab Republic","Taiwan",
        "Tajikistan","Tanzania", "United Republic of Thailand","Timor-Leste","Togo","Tokelau","Tonga","Trinidad and Tobago","Tunisia","Turkey",
        "Turkmenistan","Turks and Caicos Islands","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom of Great Britain and Northern Ireland",
        "United States of America","Uruguay","Uzbekistan","Vanuatu","Venezuela" ,"Viet Nam","Virgin Islands","Wallis and Futuna","Western Sahara",
        "Yemen","Zambia","Zimbabwe","OTHERS" ]
    }


  home_data = this.navParams.get('details');

  ionViewDidLoad() {
     }

  ionViewWillEnter() {
    
  }

  checkEmail(){
    let text_value=this.DataForm.value.email_id;
        this.sqlite.create({
            name: 'ionicdb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            var query = "SELECT email_id FROM exhibition WHERE email_id = ?";
            db.executeSql(query, [text_value])
            .then(res => {
              this.email_count=res.rows.length;
              if(this.email_count>0){
                this.DataForm.get('email_id').setErrors({
                  "Present": true
                });
              }
          }).catch(e => console.log(e));
            }).catch(e => console.log(e));          
  }

  checkMobile(){
    let text_value=this.DataForm.value.mobile_no;
        this.sqlite.create({
            name: 'ionicdb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            var query = "SELECT mobile_no FROM exhibition WHERE mobile_no = ?";
            db.executeSql(query, [text_value])
            .then(res => {
              this.mobile_count=res.rows.length;
              if(this.mobile_count>0){
                this.DataForm.get('mobile_no').setErrors({
                  "Present": true
                });
              }
          }).catch(e => console.log(e));
            }).catch(e => console.log(e));          
  }

  checkName(){
    let text_value=this.DataForm.value.name;
        this.sqlite.create({
            name: 'ionicdb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            var query = "SELECT name FROM exhibition WHERE name = ?";
            db.executeSql(query, [text_value])
            .then(res => {
              this.name_count=res.rows.length;
              if(this.name_count>0){
                this.DataForm.get('name').setErrors({
                  "Present": true
                });
              }
          }).catch(e => console.log(e));
            }).catch(e => console.log(e));          
  }

  saveData(value: any): void { 
    //checking if Form is valid
    if(this.DataForm.valid) {

      let storedItem={designation:value.designation,speciality:value.speciality,city:value.emirates}
      localStorage.setItem("store_data",JSON.stringify(storedItem));
      var country_var="";
      var nation_var="";
      //assigning Mr title to name
      var name=this.title+' '+value.name;
      
      //subsituting country with other text field
      if(value.country=="OTHERS"){
        country_var=value.other_country;
      }else{
        country_var=value.country;
      }

      //subsituting Nation with other text field
      if(value.nationality=="OTHERS"){
        nation_var=value.other_nation;
      }else{
        nation_var=value.nationality;
      }

      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      }).then((db: SQLiteObject) => {

        db.executeSql('CREATE TABLE IF NOT EXISTS exhibition(rowid INTEGER PRIMARY KEY, date TEXT, name TEXT, email_id TEXT , mobile_no TEXT, exhibition_name TEXT, hospital_name TEXT, designation TEXT, speciality TEXT, nationality TEXT, country TEXT,emirates TEXT,comments TEXT,UNIQUE(name,email_id,mobile_no))', {})
        .then(res => {console.log('Executed SQL');})
        .catch(e => console.log(JSON.stringify(e)));

        db.executeSql('INSERT INTO exhibition VALUES(NULL,?,?,?,?,?,?,?,?,?,?,?,?)',[this.home_data.date,name,value.email_id,value.mobile_no,this.home_data.exhibition_name,value.hospital_name,value.designation,value.speciality,nation_var,country_var,value.emirates,value.comments])
        .then(res => {
         
          this.toast.show('Data saved', '5000', 'center').subscribe(
            toast => {
              this.navCtrl.popToRoot();
            }
            );
        })
        .catch(e => {
          this.toast.show(JSON.stringify(e), '5000', 'center').subscribe(
            toast => {
              console.log(toast);
            }
            );
        });

      }).catch(e => {
        this.toast.show(JSON.stringify(e), '5000', 'center').subscribe(
          toast => {
            console.log(toast);
          }
          );
      });
    }
  }

  



}
