import { Component } from '@angular/core';
import { NavController,Platform,AlertController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
// import { Storage } from '@ionic/storage';

import { AddDetailsPage } from '../add-details/add-details';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import  * as FileSaver  from 'file-saver';
import { File } from '@ionic-native/file';
import { Toast } from '@ionic-native/toast';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})

export class HomePage {
	csvData: any[] = [];
	headerRow: any[] = [];

	HomeForm: FormGroup;
	exhibit:string='';
	constructor(public navCtrl: NavController,private alertCtrl: AlertController,private sqlite: SQLite,private file: File,public platform: Platform,public formBuilder: FormBuilder,private toast: Toast) {

		this.HomeForm = formBuilder.group({

			date: [{value: ''}, Validators.compose([Validators.required])],
			exhibition: [{value: ''}, Validators.compose([Validators.required])]
		});

	}

	home_data = { date:"", exhibition_name:""};
	exhibition: any = [];
	date_today:any=new Date().toISOString();
	
	expenses: any = [];
	isDisabled:boolean=true;


	ionViewDidLoad() {
		this.getData();
		if(localStorage.getItem("exhibition_name")!=""){
			this.exhibit=localStorage.getItem("exhibition_name");
		}
	}

	ionViewWillEnter() {
		this.getData();
	}

	public save(fileDestiny, fileName, fileMimeType, fileData) {
		let blob = new Blob([fileData], {type: fileMimeType});

		if (!this.platform.is('android')) {
			FileSaver.saveAs(blob, fileName); 
		}else {
			this.file.writeFile(fileDestiny, fileName, blob, { replace: true}).then(()=> {
				alert("file created at: " + fileDestiny + "/" + fileName );
			}).catch(()=>{
				alert("error creating file at :" + fileDestiny);
			})
		}
	}

	public getStorageDirectory():string {
		let src:string = "";

		if (this.platform.is('android')) {
			src = this.file.externalDataDirectory;
		}  

		return src;          
	}


	downloadCSV(){
		this.getData();
		if(this.expenses.length>0){
			var finalCSV = this.convertToCSV(this.expenses);
			this.save(this.getStorageDirectory(), "export.xls", "application/vnd.ms-excel", finalCSV);
		}else{
			this.toast.show('No Data Available', '5000', 'center').subscribe(
				toast => {
					console.log(toast);
				});
		}

	}


	convertToCSV(items) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof items != 'object' ? JSON.parse(items) : items;
    
    var CSV = '';    
    //Set Report title in first row or line

    //This condition will generate the Label/Header
    if (true) {
        var row = "";
        
        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {
            
            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);
        
        //append Label row with line break
        CSV += row + '\r\n';
    }
    
    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";
        
        //2nd loop will extract each column and convert it in string comma-seprated
        for (var indexi in arrData[i]) {
            row += '"' + arrData[i][indexi] + '",';
        }

        row.slice(0, row.length - 1);
        
        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {        
        alert("Invalid data");
        return;
    }   
    
		return (CSV);
	}


	getData() {
		this.sqlite.create({
			name: 'ionicdb.db',
			location: 'default'
		}).then((db: SQLiteObject) => {

			db.executeSql('CREATE TABLE IF NOT EXISTS exhibition(rowid INTEGER PRIMARY KEY, date TEXT, name TEXT, email_id TEXT , mobile_no TEXT, exhibition_name TEXT, hospital_name TEXT, designation TEXT, speciality TEXT, nationality TEXT, country TEXT,emirates TEXT,comments TEXT,UNIQUE(name,email_id,mobile_no))', {})
			.then(res => {console.log('Executed SQL');})
			.catch(e => console.log(JSON.stringify(e)));

			db.executeSql('SELECT * FROM exhibition ORDER BY rowid DESC', {})
			.then(res => {
				this.expenses = [];
				for(var i=0; i<res.rows.length; i++) {
					this.expenses.push({SL_NO:res.rows.item(i).rowid,DATE:res.rows.item(i).date.slice(0,10),NAME:res.rows.item(i).name,EMAIL_ID:res.rows.item(i).email_id,MOBILE_NO:res.rows.item(i).mobile_no,EXHIBITION_NAME:res.rows.item(i).exhibition_name,DESIGNATION:res.rows.item(i).designation,HOSPITAL_NAME:res.rows.item(i).hospital_name,SPECIALITY:res.rows.item(i).speciality,NATIONALITY:res.rows.item(i).nationality,COUNTRY:res.rows.item(i).country,CITY:res.rows.item(i).emirates,COMMENTS:res.rows.item(i).comments})
				}
			})
			.catch(e => console.log(e));
		}).catch(e => console.log(e));
	}

	DeleteData(){
		let alert = this.alertCtrl.create({
			title: 'Enter Password',
			inputs: [
			{
				name: 'password',
				placeholder: 'Password',
				type: 'password'
			}
			],
			buttons: [
			{
				text: 'Cancel',
				role: 'cancel',
				handler: data => {
					console.log('Cancel clicked');
				}
			},
			{
				text: 'Login',
				handler: data => {
					if (data.password=='s@ra') {
						this.sqlite.create({
							name: 'ionicdb.db',
							location: 'default'
						}).then((db: SQLiteObject) => {

							db.executeSql('DROP TABLE exhibition',{})
							.then(res => {
								localStorage.clear();
								this.exhibit='';
								this.toast.show('Successfully Deleted', '5000', 'center').subscribe(
									toast => {
										console.log(toast);
									});
							})
							.catch(e => console.log(JSON.stringify(e)));
						}).catch(e => console.log(e));
					}else {
						this.toast.show('Wrong Password', '5000', 'center').subscribe(
							toast => {
								console.log(toast);
							});
						return false;
					}
				}
			}
			]
		});
		alert.present();
	}

	edit(){
		let alert = this.alertCtrl.create({
			title: 'Enter Password',
			inputs: [
			{
				name: 'password',
				placeholder: 'Password',
				type: 'password'
			}
			],
			buttons: [
			{
				text: 'Cancel',
				role: 'cancel',
				handler: data => {
					console.log('Cancel clicked');
				}
			},
			{
				text: 'Login',
				handler: data => {
					if (data.password=='s@ra') {
						this.isDisabled=false;
						this.date_today;
					} else {
						this.toast.show('Wrong Password', '5000', 'center').subscribe(
							toast => {
								console.log(toast);
							});
						return false;
					}
				}
			}
			]
		});
		alert.present();
	}

	saveHomeData(value: any): void { 
    //checking if Form is valid
		if(this.HomeForm.valid) {
			this.home_data.date=value.date;
			this.home_data.exhibition_name=value.exhibition;
			this.isDisabled=true;
			localStorage.setItem("exhibition_name",value.exhibition);
			this.navCtrl.push(AddDetailsPage,{"details":this.home_data});
		} else {
			this.toast.show('Wrong Password', '5000', 'center').subscribe(
				toast => {
					console.log(toast);
				});
		}
	}

}
